const dataMusic = [
    {
        id: '1',
        artist: 'The weeknd',
        track: 'Save your tears',
        poster: 'img/album-01.jpg',
        mp3: 'audio/The Weeknd - Save Your Tears.mp3',
    },
    {
        id: '2',
        artist: 'Imagine Dragons',
        track: 'Follow You',
        poster: 'img/album-02.jpg',
        mp3: 'audio/Imagine Dragons - Follow You.mp3',
    },
    {
        id: '3',
        artist: 'Tove Lo',
        track: 'How Long',
        poster: 'img/album-03.jpg',
        mp3: 'audio/Tove Lo - How Long.mp3',
    },
    {
        id: '4',
        artist: 'Tom Odell',
        track: 'Another Love',
        poster: 'img/album-04.jpg',
        mp3: 'audio/Tom Odell - Another Love.mp3',
    },
    {
        id: '5',
        artist: 'Lana Del Rey',
        track: 'Born To Die',
        poster: 'img/album-05.jpg',
        mp3: 'audio/Lana Del Rey - Born To Die.mp3',
    },
    {
        id: '6',
        artist: 'Adele',
        track: 'Hello',
        poster: 'img/album-06.jpg',
        mp3: 'audio/Adele - Hello.mp3',
    },
    {
        id: '7',
        artist: 'Tom Odell',
        track: "Can't Pretend",
        poster: 'img/album-07.jpg',
        mp3: "audio/Tom Odell - Can't Pretend.mp3",
    },
    {
        id: '8',
        artist: 'Lana Del Rey',
        track: 'Young And Beautiful',
        poster: 'img/album-08.jpg',
        mp3: 'audio/Lana Del Rey - Young And Beautiful.mp3',
    },
    {
        id: '9',
        artist: 'Adele',
        track: 'Someone Like You',
        poster: 'img/album-09.jpg',
        mp3: 'audio/Adele - Someone Like You.mp3',
    },
    {
        id: '10',
        artist: 'Imagine Dragons',
        track: 'Natural',
        poster: 'img/album-10.jpg',
        mp3: 'audio/Imagine Dragons - Natural.mp3',
    },
    {
        id: '11',
        artist: 'Drake',
        track: 'Laugh Now Cry Later',
        poster: 'img/album-11.jpg',
        mp3: 'audio/Drake - Laugh Now Cry Later.mp3',
    },
    {
        id: '12',
        artist: 'Madonna',
        track: 'Frozen',
        poster: 'img/album-12.jpg',
        mp3: 'audio/Madonna - Frozen.mp3',
    },
];

let playlist = [];

const favoriteList = localStorage.getItem("favorite")
    ? JSON.parse(localStorage.getItem("favorite"))
    : [];

const audio = new Audio();

const favotiteBtn = document.querySelector(".header__favorite-btn");
const stopBtn = document.querySelector(".player__controller-stop");
const prevBtn = document.querySelector(".player__controller-prev");
const playPauseBtn = document.querySelector(".player__controller-pause");
const nextBtn = document.querySelector(".player__controller-next");
const likeBtn = document.querySelector(".player__controller-like");
const muteBtn = document.querySelector(".player__controller-mute");

const headerLogo = document.querySelector(".header__logo");

const catalogContainer = document.querySelector(".catalog__container");
// To get all cards dynamically ith this collection will be updated
const trackCards = document.getElementsByClassName("track");

const player = document.querySelector(".player");
let activeTrack;

const catalogAddBtn = document.createElement("button");
catalogAddBtn.classList.add("catalog__btn-add");
catalogAddBtn.innerHTML = `
    <span>Увидеть все</span>
    <svg width="24" height="24" viewBox="0 0 24 24" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
        <path d="M8.59 16.59L13.17 12L8.59 7.41L10 6L16 12L10 18L8.59 16.59Z" />
    </svg>
`;

const playerProgressInput = document.querySelector(".player__progress-input")
const playerTimePassed = document.querySelector(".player_time-passed");
const playerTimeTotal = document.querySelector(".player_time-total");
const playerVolumeInput = document.querySelector(".player__volume-input"); 

const createCard = (data) => {
    const card = document.createElement("a");
    card.href = "#";
    card.classList.add("catalog__item", "track");
    card.dataset.idTrack = data.id;

    card.innerHTML = `
        <div class="track__img-wrap">
        <img class="track__poster" src="${data.poster}" alt="${data.artist} - ${data.track}" width="180"
            height="180">
        </div>
        <div class="track__info track-info">
            <p class="track-info__title">${data.track}</p>
            <p class="track-info__artitst">${data.artist}</p>
        </div>
    `;

    return card;
}

const renderCatalog = (dataList) => {
    // copy
    playlist = [...dataList];
    // Clear calalog contents
    while (catalogContainer.firstChild) {
        catalogContainer.removeChild(catalogContainer.lastChild);
    }

    const listCards = dataList.map(createCard);
    catalogContainer.append(...listCards);
}


const checkCount = (i = 1) => {
    if (catalogContainer.clientHeight > trackCards[0].clientHeight * 3) {
        trackCards[trackCards.length - i].style.display = "none";
        checkCount(i + 1);
    } else if (i !== 1) {
        catalogContainer.append(catalogAddBtn);
    }
}

(() => {
    audio.volume = localStorage.getItem("volume") || 1;
    playerVolumeInput.value = audio.volume * 100;

    renderCatalog(dataMusic);
    checkCount();
})();

const playMusic = e => {
    e.preventDefault();

    const card = e.target.closest(".track");
    if (!card) {
        return;
    }
    const isPlaying = card.classList.contains("track_active");
    if (isPlaying) {
        playPause();
        return;
    }

    activeTrack = card;

    let i = 0;
    const id = card.dataset.idTrack;

    const index = favoriteList.indexOf(id);
    if (index !== -1) {
        likeBtn.classList.add("player__icon_like_active");
    } else {
        likeBtn.classList.remove("player__icon_like_active");
    }

    localStorage.setItem("favorite", JSON.stringify(favoriteList));

    const track = playlist.find((item, index) => {
        i = index;
        return id === item.id;
    });

    audio.src = track.mp3;
    audio.play();

    playPauseBtn.classList.remove("player__icon_play");
    player.classList.add("player_active");

    const prevTrack = i === 0 ? playlist.length - 1 : i - 1;
    const nextTrack = i + 1 === playlist.length ? 0 : i + 1;
    prevBtn.dataset.idTrack = playlist[prevTrack].id;
    nextBtn.dataset.idTrack = playlist[nextTrack].id;
    likeBtn.dataset.idTrack = id;

    //resetCards();
    for (let i = 0; i < trackCards.length; i++){
        if (id == trackCards[i].dataset.idtrack) {
            trackCards[i].classList.add("track_active");
        } else {
            trackCards[i].classList.remove("track_active");
        }
    }

    card.classList.add("track_active");
}

const playPause = () => {
    if (audio.paused) {
        audio.play();
    } else {
        audio.pause();
    }

    playPauseBtn.classList.toggle("player__icon_play");
    activeTrack.classList.toggle("track_paused");
}

const stopMusic = () => {
    audio.pause();
    audio.src = '';

    player.classList.remove("player_active");

    resetCards();
}

const updateTime = () => {
    const duration = audio.duration;
    const currentTime = audio.currentTime;
    const progress = (currentTime / duration) * playerProgressInput.max;

    playerProgressInput.value = progress ? progress : 0;

    const minutesPassed = Math.floor(currentTime / 60) || '0';
    const secondsPassed = Math.floor(currentTime % 60) || '0';

    const minutesDuration = Math.floor(duration / 60) || '0';
    const secondsDuration = Math.floor(duration % 60) || '0';

    playerTimePassed.textContent = `${minutesPassed}:${secondsPassed < 10 ? '0' + secondsPassed : secondsPassed}`;
    playerTimeTotal.textContent = `${minutesDuration}:${secondsDuration < 10 ? '0' + secondsDuration : secondsDuration}`;
}

favotiteBtn.addEventListener("click", () => {
    const data = dataMusic.filter(item => favoriteList.includes(item.id));
    renderCatalog(data);
    checkCount();
});

headerLogo.addEventListener("click", () => {
    renderCatalog(dataMusic);
    checkCount();
});

likeBtn.addEventListener("click", () => {
    const index = favoriteList.indexOf(likeBtn.dataset.itTrack);
    console.log(index);
    if (index === -1) {
        favoriteList.push(likeBtn.dataset.itTrack);
        likeBtn.classList.add("player__icon_like_active");
    } else {
        favoriteList.splice(index, 1);
        likeBtn.classList.remove("player__icon_like_active");
    }

    localStorage.setItem("favorite", JSON.stringify(favoriteList));
});

catalogContainer.addEventListener("click", playMusic)
playPauseBtn.addEventListener("click", playPause);
stopBtn.addEventListener("click", stopMusic);
catalogAddBtn.addEventListener("click", () => {
    [...trackCards].forEach(element => {
        element.style.display = '';
        catalogAddBtn.remove();
    });
})

prevBtn.addEventListener("click", playMusic);
nextBtn.addEventListener("click", playMusic);

audio.addEventListener("timeupdate", updateTime);
audio.addEventListener("ended", () => {
    nextBtn.dispatchEvent(new Event("click", {bubbles: true}));
});

playerProgressInput.addEventListener("change", (e) => {
    const progress = e.currentTarget.value;
    audio.currentTime = (progress / playerProgressInput.max) * audio.duration;
});

muteBtn.addEventListener("click", () => {
    if (audio.volume) {
        localStorage.setItem("volume", audio.volume);
        audio.volume = 0;
        playerVolumeInput.value = 0;
        muteBtn.classList.add("player__icon_mute-off");
    } else {
        audio.volume = localStorage.getItem("volume");
        muteBtn.classList.remove("player__icon_mute-off");
        playerVolumeInput.value = audio.volume * 100;
    }
});

playerVolumeInput.addEventListener("input", (e) => {
    const value = playerVolumeInput.value;
    audio.volume = value / 100;
});

function resetCards() {

    for (let i = 0; i < trackCards.length; i++) {
        const element = trackCards[i];
        element.classList.remove("track_active");
    }
}